// 各城邦的的金流資料

// **命名一個新變數叫 data 並宣告他是個陣列
var data = []
// 定義 轉帳記錄 的url
var record = "https://bboa14171205.nctu.me/api/web/shop/watch"

// 定義 轉帳 的url
var transfer = "https://bboa14171205.nctu.me/api/web/shop/transfer"

// 定義 登出 的 url
// var log_out = "https://bd0c7302.ngrok.io/api/shop/watch"

// get cookie
cookie = document.cookie.split("=");

// 一進畫面就先讀取一次第一筆資料
$(document).ready(function(){
    console.log(cookie)
    get_record();
})

// 抓取 get_records
function get_record(){
    console.log('yo get record')

    $.ajax({
    url: record,
    type: 'GET',
    datatype: 'json',
    headers:{
        Authorization:`Bearer ${cookie[1]}`
    },
    success: function(json) {
        console.log(json)
        data = json["data"];
        object = json;
        slogan = json.name;
        // console.log(cookie[1]);
        $("h2 span").html(json.name);
        user_imfor();
        recordlist();
        set_slogan();
        flag_ani();
        
        setTimeout(
            function () {
                $(".btn-group").css("z-index","0");
            },3000
        )
    },
    error: function(err) { 
        console.log(err);
        alert('Failed!'); 
    },
    });

}

// 抓取 reset_records
function reset_record(){
    console.log('reset record')

    $.ajax({
    url: record,
    type: 'GET',
    datatype: 'json',
    headers:{
        Authorization:`Bearer ${cookie[1]}`
    },
    success: function(json) {
        data = json["data"];
        object = json;
        user_imfor()
        recordlist();
        console.log(data)
    },
    error: function(err) { 
        console.log(err);
        alert('Reset Failed!'); 
    },
    });

}

// list 出轉帳記錄
function recordlist() {
    console.log("do do do");
    if ( screen.width >= 1024) {
        data.forEach(
            function (array) {
                $("#record tbody").append(
                    `<tr data-toggle="collapse" href="#collapse${array["id"]}" style="cursor:pointer" class="row">
                        <th scope="row" class="col-1">${array["id"]}</th>
                        <td class="col-3">${array.remittance.name}</td>
                        <td class="col-3">${array.payee.name}</td>
                        <td class="col">＄ ${array["amount"]}</td>
                        <td class="col">＄ ${array["charging"]}</td>
                        <td class="col-2">${array["updated_at"]}</td>
                    </tr>
                    <tr class="collapse more_records row" id="collapse${array["id"]}">
                        <td class="col-1"></td>
                        <td colspan="1" class="col-3">${array.remittance.account}</td>
                        <td colspan="1" class="col-3">${array.payee.account}</td>
                        <td colspan="0" class="col"></td>
                    </tr>`
                )
            }
        )
    } else if(screen.width < 1024){
        $("#record thead").html(`<tr class="row">
        <th scope="col" class="col-2">#</th>
        <th scope="col" class="col-4">匯款方</th>
        <th scope="col" class="col-6">備註</th>`)
        data.forEach(
            function (array) {
                $("#record tbody").append(
                    `<tr data-toggle="collapse" href="#collapse${array["id"]}" style="cursor:pointer" class="row">
                        <th scope="row" class="col-2">${array["id"]}</th>
                        <td class="col-4">${array.remittance.name}</td>
                        <td class="col-6">${array.remittance.account}</td>
                    </tr>
                    <tr class="collapse more_records row" id="collapse${array["id"]}">
                        <td class="col-3">收款方</td>
                        <td class="col-3">${array.payee.name}</td>
                        <td class="col-6">${array.payee.account}</td>
                        <td class="col-3">金額</td>
                        <td class="col-9">＄ ${array["amount"]}</td>
                        <td class="col-3">費用</td>
                        <td class="col-9">＄ ${array["charging"]}</td>
                        <td class="col-3">時間</td>
                        <td class="col-9">${array["updated_at"]}</td>
                    </tr>`
                )
            }
        )
    }
}

// 轉帳資料送出
$("#transfer .btn-success").click(
    function () {
    console.log($("#inputGroupSelect03").val())
    console.log($("#howmuch").val())

    $.ajax({
    url: transfer,
    type: 'POST',
    datatype: 'json',
    headers: {
        Authorization: `Bearer ${cookie[1]}`,
        Accept: "application/json"
    },
    data:{
            account:$("#inputGroupSelect03").val(),
            amount:$("#howmuch").val(),
            isShop: 1,
        },
    success: function() { 
        alert("轉帳成功")
        document.getElementById("inputGroupSelect03").value = "0"
        document.getElementById("howmuch").value = ""
        console.log(cookie[1]);
        // recordlist();
    },
    error: function(err) { 
        console.log(err);
        alert('轉帳失敗'); 
        console.log(cookie[1]);
    },
    });

    },
)

// 切換 功能
$(".assets").click(
    function() {
        $("#record,#transfer").css("display","none");
        $("#assets").css("display","block");
    }
)
$(".transfer").click(
    function() {
        $("#assets,#record").css("display","none");
        $("#transfer").css("display","block");
    }
)
$(".record").click(
    function() {
        $("#assets,#transfer").css("display","none");
        $("#record").css("display","block");
        
        $("#record tbody").html("")
        reset_record()
    }
)
$(".btn-danger").click(
    function() {
        window.location.href='./login.html'
        document.cookie = "login_cookie = ";
        // login_out();
    }
)

// 判斷並更換 旗幟 與 標語
function set_slogan() {
    if (slogan == "Sparta") {
            $("#flag").css("background-image",`url(./pic/flag-sparta.png)`);
            alert("THIS!");
            alert("IS!!");
            alert("SPARTA!!!");
            $("#sparta").css("display","none")
    } 
    else if (slogan== "Arcadia"){
        $("#flag").css("background-image",`url(./pic/flag-arkadia.png)`);
        alert("I solemnly swear that I am up to no good!");
        $("#arcadia").css("display","none")
    } else if (slogan == "Athens"){
        $("#flag").css("background-image",`url(./pic/flag-athens.png)`);
        alert("一約既定，萬山難阻。");
        $("#athens").css("display","none")
    }
}

// 使用者基本基料 帳戶總額 等級底圖
function user_imfor() {
    $("#level").html(object.level);
    $("#assets p").html(`＄ ${object.balance}`);
    $("#level_house").css("background-image",`url(./pic/lv${object.level}.png)`);
    $("#handling").html( `${object.rate*100}%` );
    $("#ex-bar").html( `${object.transection}/ ${object.upgrade + object.transection}` );
}

// 登出清除 cookie
// function login_out(){
//     console.log('ready to log out')

//     $.ajax({
//     url: log_out,
//     type: 'GET',
//     datatype: 'json',
//     success: function(json) { 
//         alert("Success log out");
//         // data = json["data"];
//         console.log(json);
//     },
//     error: function() { alert('Failed! login'); },
//     });
// }

$("#level_house").click(
    function () {
        alert("<會員福利＞\n交易總次數 > 10，Lv.1 -> Lv.2，手續費30% -> 20%\n交易總次數 > 20， Lv.2 -> Lv.3 ，手續費 20% -> 10%\n交易總次數 > 100，免手續費 ")
    }
)

function flag_ani() {
    if ( screen.width >= 1024) {
        $("#flag").css("animation","flag_fadein 1.5s ease-in forwards");
    } else if(screen.width < 1024){
        $("#flag").css("animation","flag_fadein_mb 1.5s ease-in forwards");
    }
}