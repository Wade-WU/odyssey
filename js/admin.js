// 菲基斯的金流中心

// **命名一個新變數叫 data 並宣告他是個陣列
var data = []
// 定義 所有陣營總資產 的url
var assets = "https://bboa14171205.nctu.me/api/admin/watch"

// 定義 所有的轉帳交易紀錄 的url
var record = "https://bboa14171205.nctu.me/api/web/shop/watch"

// 定義 轉帳 的url
var transfer = "https://bboa14171205.nctu.me/api/web/shop/transfer"

// 定義 登出 的 url
// var log_out = "https://bd0c7302.ngrok.io/api/shop/watch"

// get cookie
cookie = document.cookie.split("=");


// 一進畫面就先讀取一次第一筆資料
$(document).ready(function(){
    get_assets();
    get_records();
    alert("請進，陌生人，不過你要當心，貪得無厭會是什麼下場，一味索取，不勞而獲，必將受到最嚴厲的懲罰，因此如果你想從我們的地下金庫取走一份從來不屬於你的財富，竊賊啊，你已經受到警告，當心招來的不是寶藏，而是惡報。");
})

// 抓取 assets
function get_assets(){
    $.ajax({
    url: assets,
    type: 'GET',
    datatype: 'json',
    headers: {
        Authorization:`Bearer ${cookie[1]}`
    },
    success: function(json) { 
        // alert("Success");
        console.log(json);
        data = json["data"];
        message = json.message
        level = json.message.level;
        assetslist();
        user_imfor();
        flag_ani();

        setTimeout(
            function () {
                $(".btn-group").css("z-index","0");
            },3000
        )
    },
    error: function(err) { 
        console.log(err);
        alert('Failed!'); 
    },
    });
}

// reset assets
function reset_assets(){
    $.ajax({
    url: assets,
    type: 'GET',
    datatype: 'json',
    headers: {
        Authorization:`Bearer ${cookie[1]}`
    },
    success: function(json) { 
        // alert("Success");
        console.log(json);
        data = json["data"];
        assetslist();
    },
    error: function(err) { 
        console.log(err);
        alert('Failed!'); 
    },
    });
}

// 抓取 get_records
function get_records(){

    $.ajax({
    url: record,
    type: 'GET',
    datatype: 'json',
    headers: {
        Authorization: `Bearer ${cookie[1]}`
    },
    success: function(json) { 
        // alert("Success get records");
        console.log(json);
        dota = json["data"];
        // dota = json;
        recordlist();
    },
    error: function(err) { 
        console.log(err);
        alert('Failed! get records'); 
    },
    });
}

// list 出 各國庫資料
function assetslist() {
    console.log("list 出 各國總額");
    data.forEach(
        function (array) {
            $("#assets").append(
                `<h4>${array["name"]} 國庫總資產</h4>
                <p>$ ${array["balance"]}</p>`
            )
        }
    )
}

// 轉帳資料送出
$("#transfer .btn-success").click(
    function () {
        console.log($("#inputGroupSelect03").val())
        console.log($("#howmuch").val())
    
        $.ajax({
        url: transfer,
        type: 'POST',
        datatype: 'json',
        headers: {
            Authorization: `Bearer ${cookie[1]}`,
            Accept: "application/json"
        },
        data:{
                account:$("#inputGroupSelect03").val(),
                amount:$("#howmuch").val(),
                isShop: 1,
            },
        success: function() { 
            alert("轉帳成功")
            document.getElementById("inputGroupSelect03").value = "0"
            document.getElementById("howmuch").value = ""
            console.log(cookie[1]);
            // recordlist();
        },
        error: function(err) { 
            console.log(err);
            alert('轉帳失敗'); 
            console.log(cookie[1]);
        },
        });
    
        },
)

// list 出所有轉帳記錄
function recordlist() {
    // console.log("do records");
    if ( screen.width >= 1024) {
        dota.forEach(
            function (array) {
                $("#record tbody").append(
                    `<tr data-toggle="collapse" href="#collapse${array["id"]}" style="cursor:pointer" class="row">
                        <th scope="row" class="col-1">${array["id"]}</th>
                        <td class="col-3">${array.remittance.name}</td>
                        <td class="col-3">${array.payee.name}</td>
                        <td class="col">＄ ${array["amount"]}</td>
                        <td class="col">＄ ${array["charging"]}</td>
                        <td class="col-2">${array["updated_at"]}</td>
                    </tr>
                    <tr class="collapse more_records row" id="collapse${array["id"]}">
                        <td class="col-1"></td>
                        <td colspan="1" class="col-3">${array.remittance.account}</td>
                        <td colspan="1" class="col-3">${array.payee.account}</td>
                        <td colspan="0" class="col"></td>
                    </tr>`
                )
            }
        )
    } else if(screen.width < 1024){
        $("#record thead").html(`<tr class="row">
        <th scope="col" class="col-2">#</th>
        <th scope="col" class="col-4">匯款方</th>
        <th scope="col" class="col-6">備註</th>`)
        dota.forEach(
            function (array) {
                $("#record tbody").append(
                    `<tr data-toggle="collapse" href="#collapse${array["id"]}" style="cursor:pointer" class="row">
                        <th scope="row" class="col-2">${array["id"]}</th>
                        <td class="col-4">${array.remittance.name}</td>
                        <td class="col-6">${array.remittance.account}</td>
                    </tr>
                    <tr class="collapse more_records row" id="collapse${array["id"]}">
                        <td class="col-3">收款方</td>
                        <td class="col-3">${array.payee.name}</td>
                        <td class="col-6">${array.payee.account}</td>
                        <td class="col-3">金額</td>
                        <td class="col-9">＄ ${array["amount"]}</td>
                        <td class="col-3">費用</td>
                        <td class="col-9">＄ ${array["charging"]}</td>
                        <td class="col-3">時間</td>
                        <td class="col-9">${array["updated_at"]}</td>
                    </tr>`
                )
            }
        )
    }
}

// 切換 功能
$(".assets").click(
    function() {
        $("#record,#transfer").css("display","none");
        $("#assets").css("display","block");
    }
)
$(".transfer").click(
    function() {
        $("#assets,#record").css("display","none");
        $("#transfer").css("display","block");
    }
)
$(".record").click(
    function() {
        $("#assets,#transfer").css("display","none");
        $("#record").css("display","block");

        $("#record tbody").html("")
        get_records();
        $("#assets").html("")
        reset_assets()
    }
)
$(".btn-danger").click(
    function() {
        window.location.href='./login.html'
        document.cookie = "login_cookie = ";
        // login_out();
    }
)

function user_imfor() {
    $("h2 span").html(message.name);
    $("#flag").css("background-image",`url(./pic/flag-phokis.png)`);
    $("#admin_assets").html(`＄ ${message.balance}`);
    $("#level_house").css("background-image",`url(./pic/lv${level}.png)`);
    $("#level").html(level);
    $("#phokis").css("display","none")
}

function flag_ani() {
    if ( screen.width >= 1024) {
        $("#flag").css("animation","flag_fadein 1.5s ease-in forwards");
    } else if(screen.width < 1024){
        $("#flag").css("animation","flag_fadein_mb 1.5s ease-in forwards");
    }
}

// 登出清除 cookie
// function login_out(){
//     console.log('ready to log out')

//     $.ajax({
//     url: log_out,
//     type: 'GET',
//     datatype: 'json',
//     success: function(json) { 
//         alert("Success log out");
//         // data = json["data"];
//         console.log(json);
//     },
//     error: function() { alert('Failed! login'); },
//     });
// }